/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fichamedicainfantil;

import dominio.EsquemaVacuna;
import dominio.Ninio;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pablo
 */
public class SistemaTest {
    
    public SistemaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetComponenteCadenaSeparadaPor1() {
        String unaCadena = "";
        Sistema instance = new Sistema();
        String expResult = "";
        String result = instance.getComponenteCadenaSeparadaPor(unaCadena, "  ", 1);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetComponenteCadenaSeparadaPor2() {
        String unaCadena = "Pablo  Benitez";
        Sistema instance = new Sistema();
        String expResult = "Pablo";
        String result = instance.getComponenteCadenaSeparadaPor(unaCadena, "  ", 1);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetComponenteCadenaSeparadaPor3() {
        String unaCadena = "Pablo  Benitez González";
        Sistema instance = new Sistema();
        String expResult = "Pablo";
        String result = instance.getComponenteCadenaSeparadaPor(unaCadena, "  ", 1);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetComponenteCadenaSeparadaPor4() {
        String unaCadena = "Pablo Sebastián  Benitez González";
        Sistema instance = new Sistema();
        String expResult = "Pablo Sebastián";
        String result = instance.getComponenteCadenaSeparadaPor(unaCadena, "  ", 1);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetComponenteCadenaSeparadaPor5() {
        String unaCadena = "Pablo Sebastián  Benitez González";
        Sistema instance = new Sistema();
        String expResult = "Benitez González";
        String result = instance.getComponenteCadenaSeparadaPor(unaCadena, "  ", 2);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetComponenteCadenaSeparadaPor6() {
        String unaCadena = "Pablo  Benitez González";
        Sistema instance = new Sistema();
        String expResult = "Benitez González";
        String result = instance.getComponenteCadenaSeparadaPor(unaCadena, "  ", 2);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetComponenteCadenaSeparadaPor7() {
        String unaCadena = "Pablo  Benitez";
        Sistema instance = new Sistema();
        String expResult = "Benitez";
        String result = instance.getComponenteCadenaSeparadaPor(unaCadena, "  ", 2);
        assertEquals(expResult, result);
    }
    
}
