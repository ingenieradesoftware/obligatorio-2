package dominio;

import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Clase EsquemaVacunaTest, pruebas unitarias para EsquemaVacuna.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class EsquemaVacunaTest {
    
    public EsquemaVacunaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNombre method, of class EsquemaVacuna.
     */
    @Test
    public void testGetNombre() {
        EsquemaVacuna instance = new EsquemaVacuna();
        instance.setNombre("MSP numero 1");
        String expResult = "MSP numero 1";
        
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class EsquemaVacuna.
     */
    @Test
    public void testSetNombre() {
        String nombre = "El nombre del esquema!.";
        EsquemaVacuna instance = new EsquemaVacuna();
        instance.setNombre(nombre);
        
        assertEquals(nombre, instance.getNombre());
    }

    /**
     * Test of getFechaCreado method, of class EsquemaVacuna.
     */
    @Test
    public void testGetFechaCreado() {
        EsquemaVacuna instance = new EsquemaVacuna();
        Date expResult = new Date();
        
        instance.setFechaCreado(expResult);
        Date result = instance.getFechaCreado();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of setFechaCreado method, of class EsquemaVacuna.
     */
    @Test
    public void testSetFechaCreado() {
        Date fechaCreado = new Date();
        EsquemaVacuna instance = new EsquemaVacuna();
        instance.setFechaCreado(fechaCreado);
        
        assertEquals(fechaCreado, instance.getFechaCreado());
    }

    /**
     * Test of getVacunaciones method, of class EsquemaVacuna.
     */
    @Test
    public void testGetVacunaciones() {
        ArrayList<Vacunacion> vacunacion = new ArrayList<Vacunacion>();
        vacunacion.add(new Vacunacion(11, new Date(), new ArrayList<Vacuna>()));
        
        ArrayList<Vacuna> vacs = new ArrayList<Vacuna>();
        vacs.add(new Vacuna("una vacunita"));
        vacs.add(new Vacuna("intramuscular"));
        
        vacunacion.add(new Vacunacion(8, new Date(), vacs));
        
        EsquemaVacuna instance = new EsquemaVacuna();
        
        instance.setVacunaciones(vacunacion);
        
        assertEquals(2, instance.getVacunaciones().size());
    }

    /**
     * Test of setVacunaciones method, of class EsquemaVacuna.
     */
    @Test
    public void testSetVacunaciones() {

        ArrayList<Vacunacion> vacunacion = new ArrayList<Vacunacion>();
        vacunacion.add(new Vacunacion(11, new Date(), new ArrayList<Vacuna>()));
        
        ArrayList<Vacuna> vacs = new ArrayList<Vacuna>();
        vacs.add(new Vacuna("una vacunita"));
        vacs.add(new Vacuna("intramuscular"));
        
        vacunacion.add(new Vacunacion(8, new Date(), vacs));
        
        EsquemaVacuna instance = new EsquemaVacuna();
        
        instance.setVacunaciones(vacunacion);
        
        assertEquals(2, instance.getVacunaciones().size());
        
    }

    /**
     * Test of getVacunacionParaMes method, of class EsquemaVacuna.
     */
    @Test
    public void testGetVacunacionParaMes() {
        
        int unMes = 1;
        
        ArrayList<Vacunacion> vacunacion = new ArrayList<Vacunacion>();
        
        Vacunacion vcs = new Vacunacion(unMes, new Date(), new ArrayList<Vacuna>());
        vacunacion.add(vcs);
        
        ArrayList<Vacuna> vacs = new ArrayList<Vacuna>();
        vacs.add(new Vacuna("una vacunita"));
        vacs.add(new Vacuna("intramuscular"));
        
        vacunacion.add(new Vacunacion(8, new Date(), vacs));
        
        EsquemaVacuna instance = new EsquemaVacuna();
        
        instance.setVacunaciones(vacunacion);
        
        Vacunacion vc = instance.getVacunacionParaMes(unMes);
        
        assertEquals(vcs, vc);
    }

    /**
     * Test of compareTo method, of class EsquemaVacuna.
     */
    @Test
    public void testCompareTo() {
        EsquemaVacuna uno      = new EsquemaVacuna("MSP 90210", new Date(), new ArrayList<Vacunacion>());
        EsquemaVacuna instance = new EsquemaVacuna("MSP 90210", new Date(), new ArrayList<Vacunacion>());
        int expResult = 0;
        int result = instance.compareTo(uno);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class EsquemaVacuna.
     */
    @Test
    public void testEquals() {
        EsquemaVacuna uno      = new EsquemaVacuna("MSP 9i8712398", new Date(), new ArrayList<Vacunacion>());
        EsquemaVacuna instance = new EsquemaVacuna("MSP 9i8712398", new Date(), new ArrayList<Vacunacion>());
        
        assertEquals(true, instance.equals(uno));
    }

    /**
     * Test of toString method, of class EsquemaVacuna.
     */
    @Test
    public void testToString() {
        Date fecha = new Date();
        
        String expResult = "MSP 90210 (" + fecha.toString() + ")";
        
        EsquemaVacuna instance = new EsquemaVacuna();
        instance.setNombre("MSP 90210");
        instance.setFechaCreado(fecha);
        instance.setVacunaciones(new ArrayList<Vacunacion>());
        
        String result = instance.toString();

        assertEquals(expResult, result);
    }
    
}
