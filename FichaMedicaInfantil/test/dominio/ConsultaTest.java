package dominio;

import java.time.LocalDateTime;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Clase ConsultaTest, pruebas unitarias para Consulta.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class ConsultaTest {
    
    public ConsultaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFecha method, of class Consulta.
     */
    @Test
    public void testGetFecha() {
        Consulta instance = new Consulta();
        LocalDateTime l = LocalDateTime.now();
        instance.setFecha(l);

        assertEquals(l, instance.getFecha());
    }

    /**
     * Test of getNota method, of class Consulta.
     */
    @Test
    public void testGetNota() {
        Consulta instance = new Consulta();
        instance.setNota("La nueva nota.");
        assertEquals("La nueva nota.", instance.getNota());
    }

    /**
     * Test of isAsistencia method, of class Consulta.
     */
    @Test
    public void testIsAsistencia() {
        Consulta instance = new Consulta();
        boolean expResult = false;
        instance.setAsistencia(false);
        assertEquals(expResult, instance.isAsistencia());
    }

    /**
     * Test of getPeso method, of class Consulta.
     */
    @Test
    public void testGetPeso() {
        Consulta instance = new Consulta();
        float expResult = (float)4.850;
        instance.setPeso(expResult);
        float result = instance.getPeso();
        assertEquals(expResult, result, 0.0001);
    }

    /**
     * Test of getAltura method, of class Consulta.
     */
    @Test
    public void testGetTalla() {
        Consulta instance = new Consulta();
        int expResult = 65;
        instance.setTalla(65);
        int result = instance.getTalla();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPerimetroCraneal method, of class Consulta.
     */
    @Test
    public void testGetPerimetroCraneal() {
        Consulta instance = new Consulta();
        float expResult = (float)38.65;
        instance.setPerimetroCraneal(expResult);
        float result = instance.getPerimetroCraneal();
        assertEquals(expResult, result, 0.0001);
    }

    /**
     * Test of setFecha method, of class Consulta.
     */
    @Test
    public void testSetFecha() {
        LocalDateTime fecha = LocalDateTime.now();
        Consulta instance = new Consulta();
        instance.setFecha(fecha);
        
        assertEquals(fecha, instance.getFecha());
    }

    /**
     * Test of setNota method, of class Consulta.
     */
    @Test
    public void testSetNota() {
        String nota = "Hoy es la consulta del bebé.";
        Consulta instance = new Consulta();
        instance.setNota(nota);
        
        assertEquals(nota, instance.getNota());
    }

    /**
     * Test of setAsistencia method, of class Consulta.
     */
    @Test
    public void testSetAsistencia() {
        boolean asistencia = true;
        Consulta instance  = new Consulta();
        instance.setAsistencia(asistencia);
        
        assertEquals(true, instance.isAsistencia());
    }

    /**
     * Test of setPeso method, of class Consulta.
     */
    @Test
    public void testSetPeso() {
        float peso = (float)8.6983;
        Consulta instance = new Consulta();
        instance.setPeso(peso);
        
        assertEquals(peso, instance.getPeso(), 0.0001);
        
    }

    /**
     * Test of setAltura method, of class Consulta.
     */
    @Test
    public void testSetTalla() {
        int altura = 89;
        Consulta instance = new Consulta();
        instance.setTalla(altura);
        
        assertEquals(altura, instance.getTalla());
    }

    /**
     * Test of setPerimetroCraneal method, of class Consulta.
     */
    @Test
    public void testSetPerimetroCraneal() {
        float perimetroCraneal = (float)38.650;
        Consulta instance = new Consulta();
        instance.setPerimetroCraneal(perimetroCraneal);
        
        assertEquals(perimetroCraneal, instance.getPerimetroCraneal(), 0.0001);
    }
    
    @Test
    public void testCompareTo() {
        Consulta unaConsulta = new Consulta();
        unaConsulta.setAsistencia(true);
        unaConsulta.setEdad(6);
        
        LocalDateTime fechaNac1 = LocalDateTime.of(1983, Month.AUGUST, 8, 23, 45);
        unaConsulta.setFecha(fechaNac1);
        unaConsulta.setNota("La notita.");
        unaConsulta.setPerimetroCraneal((float)34.68);
        unaConsulta.setPeso((float)4.856);
        unaConsulta.setTalla(95);
        
        Consulta instance = new Consulta(fechaNac1, "La notita.", true, (float)4.856, 95, (float)34.68, 6);
        
        int expResult = 0;
        int result = instance.compareTo(unaConsulta);

        assertEquals(expResult, result);
    }
    
    @Test
    public void testToString() {
        Consulta instance = new Consulta();
        LocalDateTime expResult = LocalDateTime.now();
        instance.setFecha(expResult);
        String result = instance.toString();
        assertEquals(expResult.toString(), result);
    }
    
}
