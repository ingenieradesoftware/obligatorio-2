package dominio;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Clase NinioTest, pruebas unitarias para Ninio.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class NinioTest {
    
    public NinioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNombres method, of class Ninio.
     */
    @Test
    public void testGetNombres() {
        Ninio instance = new Ninio();
        instance.setNombres("Jean Pierre");
        String expResult = "Jean Pierre";
        String result = instance.getNombres();
        assertEquals(expResult, result);
    }

    /**
     * Test of getApellidos method, of class Ninio.
     */
    @Test
    public void testGetApellidos() {
        Ninio instance = new Ninio();
        instance.setApellidos("Maradona Franco");
        String expResult = "Maradona Franco";
        String result = instance.getApellidos();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFechaNacimiento method, of class Ninio.
     */
    @Test
    public void testGetFechaNacimiento() {
        Ninio instance = new Ninio();
        Date fechaDeNacimiento = new Date(1983, 8, 8);
        instance.setFechaNacimiento(fechaDeNacimiento);
        Date expResult = fechaDeNacimiento;
        Date result = instance.getFechaNacimiento();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCedula method, of class Ninio.
     */
    @Test
    public void testGetCedula() {
        Ninio instance = new Ninio();
        instance.setCedula("349110286");
        String expResult = "349110286";
        String result = instance.getCedula();
        assertEquals(expResult, result);
    }

    /**
     * Test of getConsultas method, of class Ninio.
     */
    @Test
    public void testGetConsultas() {
        Ninio instance = new Ninio();
        ArrayList<Consulta> expResult = new ArrayList<Consulta>();
        ArrayList<Consulta> result = instance.getConsultas();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEsquemaVacunacion method, of class Ninio.
     */
    @Test
    public void testGetEsquemaVacunacion() {
        Ninio instance = new Ninio();
        EsquemaVacuna expResult = new EsquemaVacuna();
        EsquemaVacuna result = instance.getEsquemaVacunacion();
        assertEquals(expResult, result);
    }

    /**
     * Test of addConsulta method, of class Ninio.
     */
    @Test
    public void testAddConsulta() {
        Consulta unaConsulta = new Consulta();
        Ninio instance = new Ninio();
        instance.addConsulta(unaConsulta);
        
        int cantConsultas = instance.getConsultas().size();
        
        assertEquals(1, cantConsultas);
    }

    /**
     * Test of getConsultasPasadas method, of class Ninio.
     */
    @Test
    public void testGetConsultasPasadas() {
        Ninio instance = new Ninio();
        
        Consulta c1 = new Consulta();
        c1.setAsistencia(true);
        c1.setEdad(6);
        c1.setFecha(LocalDateTime.of(2015, Month.MARCH, 10, 9, 30));
        c1.setNota("una nota");
        c1.setPerimetroCraneal((float)38);
        c1.setPeso((float)15);
        c1.setTalla(89);
        
        Consulta c2 = new Consulta();
        c2.setAsistencia(true);
        c2.setEdad(6);
        c2.setFecha(LocalDateTime.of(2015, Month.DECEMBER, 10, 9, 30));
        c2.setNota("una nota");
        c2.setPerimetroCraneal((float)38);
        c2.setPeso((float)15);
        c2.setTalla(89);        
        
        instance.addConsulta(c1);
        instance.addConsulta(c2);
        
        assertEquals(c1, instance.getConsultasPasadas().get(0));

    }

    /**
     * Test of getConsultasFuturas method, of class Ninio.
     */
    @Test
    public void testGetConsultasFuturas() {
        Ninio instance = new Ninio();
        
        Consulta c1 = new Consulta();
        c1.setAsistencia(true);
        c1.setEdad(6);
        c1.setFecha(LocalDateTime.of(2015, Month.MARCH, 10, 9, 30));
        c1.setNota("una nota");
        c1.setPerimetroCraneal((float)38);
        c1.setPeso((float)15);
        c1.setTalla(89);
        
        Consulta c2 = new Consulta();
        c2.setAsistencia(true);
        c2.setEdad(6);
        c2.setFecha(LocalDateTime.of(2015, Month.DECEMBER, 10, 9, 30));
        c2.setNota("una nota");
        c2.setPerimetroCraneal((float)38);
        c2.setPeso((float)15);
        c2.setTalla(89);        
        
        instance.addConsulta(c1);
        instance.addConsulta(c2);
        
        assertEquals(c2, instance.getConsultasFuturas().get(0));
    }

    /**
     * Test of getTodasLasVacunas method, of class Ninio.
     */
    @Test
    public void testGetTodasLasVacunas() {
        Ninio instance = new Ninio();
        ArrayList<Vacuna> expResult = new ArrayList<Vacuna>();
        ArrayList<Vacuna> result = instance.getTodasLasVacunas();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTodasLasVacunas1() {
        ArrayList<Vacunacion> vacunacion = new ArrayList<Vacunacion>();
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("alhpa"));
        vacunas.add(new Vacuna("beta caroteno"));
        Vacunacion v = new Vacunacion(8, new Date(), vacunas);
        vacunacion.add(v);
        EsquemaVacuna esquema = new EsquemaVacuna("OMS 98123", new Date(), vacunacion);
        
        Ninio instance = new Ninio();
        instance.setEsquemaVacunacion(esquema);
        
        ArrayList<Vacuna> expResult = vacunas;
        ArrayList<Vacuna> result = instance.getTodasLasVacunas();
        assertEquals(expResult, result);
    }    
    
    /**
     * Test of equals method, of class Ninio.
     */
    @Test
    public void testEquals() {
        Object obj = new Ninio();
        Ninio instance = new Ninio();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Ninio.
     */
    @Test
    public void testEquals1() {
        Ninio obj = new Ninio("Edinson Roberto", "Cavani Gómez", new Date(1987, 2, 14), "34991039", new ArrayList<Consulta>(), new EsquemaVacuna());
        Ninio instance = new Ninio();
        instance.setNombres("Edinson");
        instance.setApellidos("Cavani");
        instance.setFechaNacimiento(new Date(1987, 2, 14));
        instance.setCedula("34991039");
        instance.setConsultas(new ArrayList<Consulta>());
        instance.setEsquemaVacunacion(new EsquemaVacuna());
                
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }    
    
    /**
     * Test of equals method, of class Ninio.
     */
    @Test
    public void testEquals2() {
        Ninio obj = new Ninio("Edinson Roberto", "Cavani Gómez", new Date(1987, 2, 14), "34991039", new ArrayList<Consulta>(), new EsquemaVacuna());
        Ninio instance = new Ninio();
        instance.setNombres("Edinson Roberto");
        instance.setApellidos("Cavani");
        instance.setFechaNacimiento(new Date(1987, 2, 14));
        instance.setCedula("34991039");
        instance.setConsultas(new ArrayList<Consulta>());
        instance.setEsquemaVacunacion(new EsquemaVacuna());
                
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }    
    
    /**
     * Test of compareTo method, of class Ninio.
     */
    @Test
    public void testCompareTo() {
        Ninio unNinio = new Ninio();
        unNinio.setApellidos("Gomez Bolaños");
        unNinio.setCedula("873991208");
        unNinio.setConsultas(new ArrayList<Consulta>());
        unNinio.setEsquemaVacunacion(new EsquemaVacuna());
        unNinio.setFechaNacimiento(new Date(1987, 1, 22));
        unNinio.setNombres("Roberto");
        
        Ninio instance = new Ninio("Roberto", "Gomez Bolaños", new Date(1987, 1, 22), "873991208", new ArrayList<Consulta>(), new EsquemaVacuna());
        
        int expResult = 0;
        int result = instance.compareTo(unNinio);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Ninio.
     */
    @Test
    public void testToString() {
        Ninio instance = new Ninio();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testToString1() {
        Ninio instance = new Ninio();
        instance.setApellidos("Euler");
        instance.setNombres("Leonard");
        instance.setCedula("15170718");
        
        String expResult = "Leonard Euler (15170718)";
        String result = instance.toString();
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testToString2() {
        Ninio instance = new Ninio();
        instance.setApellidos("Dijkstra");
        instance.setNombres("Edsger Wybe");

        String expResult = "Edsger Wybe Dijkstra";
        String result = instance.toString();
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetConsultaParaFecha() {
        Ninio instance = new Ninio();
        instance.setApellidos("Dijkstra");
        instance.setNombres("Edsger Wybe");

        LocalDateTime ahora = LocalDateTime.now();
        
        Consulta c  = new Consulta(ahora, "", true, 0, 0, 0, 0);
        Consulta c1 = new Consulta(LocalDateTime.of(2015, 6, 11, 11, 15), "", true, 0, 0, 0, 0);
        
        ArrayList<Consulta> consultas = new ArrayList<Consulta>();
        consultas.add(c1);
        
        instance.setConsultas(consultas);
        
        instance.getConsultas().add(c);
        
        Consulta expResult = c;
        Consulta result    = instance.getConsultaParaFecha(ahora);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetConsultaParaFechaNoEncontrada() {
        Ninio instance = new Ninio();
        instance.setApellidos("Dijkstra");
        instance.setNombres("Edsger Wybe");

        LocalDateTime ahora = LocalDateTime.now();
        
        Consulta c  = new Consulta(ahora, "", true, 0, 0, 0, 0);
        Consulta c1 = new Consulta(LocalDateTime.of(2013, 6, 11, 11, 15), "", true, 0, 0, 0, 0);
        
        ArrayList<Consulta> consultas = new ArrayList<Consulta>();
        consultas.add(c1);
        
        instance.setConsultas(consultas);
        
        instance.getConsultas().add(c);
        
        Consulta expResult = c;
        Consulta result    = instance.getConsultaParaFecha(LocalDateTime.of(2016, 6, 11, 11, 15));
        
        assertEquals(false, c1.equals(result));
    }    
    
    @Test
    public void testActualizarConsulta() {
        Ninio instance = new Ninio();
        instance.setApellidos("Dijkstra");
        instance.setNombres("Edsger Wybe");

        LocalDateTime ahora = LocalDateTime.now();
        
        Consulta c  = new Consulta(ahora, "c notas", true, (float) 1.2, 80, 27, 1);
        Consulta c1 = new Consulta(LocalDateTime.of(2015, 10, 11, 14, 30), "c1, tiene notas.", true, 0, 0, 0, 0);
        
        ArrayList<Consulta> consultas = new ArrayList<Consulta>();
        consultas.add(c1);
        
        instance.setConsultas(consultas);
        
        instance.getConsultas().add(c);
        
        Consulta update = new Consulta(ahora, "Nuevas notas!", true, (float) 1.5, 100, 32, 3);
        
        instance.actualizarConsulta(update);
        
        Consulta result = instance.getConsultaParaFecha(ahora);
        
        assertEquals(update.getNota(), result.getNota());
        assertEquals(update.isAsistencia(), result.isAsistencia());
        assertEquals(update.getPeso(), result.getPeso(), 0.001);
    }
    
     @Test
    public void testActualizarConsultaNoActualiza() {
        Ninio instance = new Ninio();
        instance.setApellidos("Dijkstra");
        instance.setNombres("Edsger Wybe");

        LocalDateTime ahora = LocalDateTime.now();
        
        Consulta c  = new Consulta(ahora, "c notas", true, (float) 1.2, 80, 27, 1);
        Consulta c1 = new Consulta(LocalDateTime.of(2015, 10, 11, 14, 30), "c1, tiene notas.", true, 0, 0, 0, 0);
        
        ArrayList<Consulta> consultas = new ArrayList<Consulta>();
        consultas.add(c1);
        
        instance.setConsultas(consultas);
        
        instance.getConsultas().add(c);
        
        Consulta update = new Consulta(LocalDateTime.of(2012, 10, 11, 14, 30), "Nuevas notas!", true, (float) 1.5, 100, 32, 3);
        
        instance.actualizarConsulta(update);
        
        Consulta result = instance.getConsultaParaFecha(ahora);

        assertNotEquals(update.getNota(), result.getNota());
        assertEquals(update.isAsistencia(), result.isAsistencia());
        assertNotEquals(update.getPeso(), result.getPeso(), 0.001);
    }   
    
    @Test
    public void testGetVacunacionPorMes() {
        
        ArrayList<Vacunacion> vacunacion = new ArrayList<Vacunacion>();
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("alhpa"));
        vacunas.add(new Vacuna("beta caroteno"));
        Vacunacion v = new Vacunacion(8, new Date(), vacunas);
        vacunacion.add(v);
        EsquemaVacuna esquema = new EsquemaVacuna("OMS 98123", new Date(), vacunacion);
        
        Ninio instance = new Ninio();
        instance.setEsquemaVacunacion(esquema);

        Vacunacion vc = instance.getVacunacionPorMes(8);
        
        assertEquals(v, vc);
        
    }
    
    @Test
    public void testGetVacunacionPorMesNoEncuentra() {
        
        ArrayList<Vacunacion> vacunacion = new ArrayList<Vacunacion>();
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("alhpa"));
        vacunas.add(new Vacuna("beta caroteno"));
        Vacunacion v = new Vacunacion(8, new Date(), vacunas);
        vacunacion.add(v);
        EsquemaVacuna esquema = new EsquemaVacuna("OMS 98123", new Date(), vacunacion);
        
        Ninio instance = new Ninio();
        instance.setEsquemaVacunacion(esquema);

        Vacunacion vc = instance.getVacunacionPorMes(10);
        
        assertEquals(new Vacunacion(), vc);
        
    }    
    
    @Test
    public void testAdministrarVacuna() {
        ArrayList<Vacunacion> vacunacion = new ArrayList<Vacunacion>();
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("alhpa"));
        vacunas.add(new Vacuna("beta caroteno"));
        Vacunacion v = new Vacunacion(8, new Date(), vacunas);
        vacunacion.add(v);
        EsquemaVacuna esquema = new EsquemaVacuna("OMS 98123", new Date(), vacunacion);
        
        Ninio instance = new Ninio();
        instance.setEsquemaVacunacion(esquema);

        Vacunacion v1 = new Vacunacion(8, new Date(2015-1900, Calendar.OCTOBER, 7, 15, 45), new ArrayList<Vacuna>());
        instance.administrarVacuna(v1);
        
        
        assertEquals(instance.getVacunacionPorMes(8).getFechaAdministradas(), v1.getFechaAdministradas());
    }

    @Test
    public void testAdministrarVacunaNoExiste() {
        ArrayList<Vacunacion> vacunacion = new ArrayList<Vacunacion>();
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("alhpa"));
        vacunas.add(new Vacuna("beta caroteno"));
        Vacunacion v = new Vacunacion(8, new Date(), vacunas);
        vacunacion.add(v);
        EsquemaVacuna esquema = new EsquemaVacuna("OMS 98123", new Date(), vacunacion);
        
        Ninio instance = new Ninio();
        instance.setEsquemaVacunacion(esquema);

        Vacunacion v1 = new Vacunacion(10, new Date(2015-1900, Calendar.OCTOBER, 7, 15, 45), new ArrayList<Vacuna>());
        instance.administrarVacuna(v1);
        
        
        assertNotEquals(instance.getVacunacionPorMes(8).getFechaAdministradas(), v1.getFechaAdministradas());
    }    

    /**
     * OjO si este test no lo corrés en Noviembre no da verde!!!
     */    
    @Test
    public void testGetEdadEnMeses() {
        Ninio instance = new Ninio();
        instance.setFechaNacimiento(new Date(2015-1900, Calendar.MARCH, 7, 15, 45));
        
        assertEquals(8, instance.getEdadEnMeses());
        
    }
    
    /**
     * OjO si este test no lo corrés en Noviembre no da verde!!!
     */
    @Test
    public void testGetEdadEnMeses1() {
        Ninio instance = new Ninio();
        instance.setFechaNacimiento(new Date(2015-1900, Calendar.NOVEMBER, 7, 15, 45));
        
        assertEquals(0, instance.getEdadEnMeses());
        
    }    
}
