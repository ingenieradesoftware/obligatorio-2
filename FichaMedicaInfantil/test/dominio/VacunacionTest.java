package dominio;

import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Clase VacunacionTest, pruebas unitarias para Vacunacion.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class VacunacionTest {
    
    public VacunacionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setFechaAdministradas method, of class Vacunacion.
     */
    @Test
    public void testSetFechaAdministradas() {
        Date unaFecha = new Date();
        Vacunacion instance = new Vacunacion();
        instance.setFechaAdministradas(unaFecha);
        
        assertEquals(unaFecha, instance.getFechaAdministradas());
    }

    /**
     * Test of getFechaAdministradas method, of class Vacunacion.
     */
    @Test
    public void testGetFechaAdministradas() {
        Date unaFecha = new Date();
        Vacunacion instance = new Vacunacion();
        instance.setFechaAdministradas(unaFecha);
        
        assertEquals(unaFecha, instance.getFechaAdministradas());
    }

    /**
     * Test of getMes method, of class Vacunacion.
     */
    @Test
    public void testGetMes() {
        Vacunacion instance = new Vacunacion();
        int expResult = 6;
        instance.setMes(6);
        int result = instance.getMes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMes method, of class Vacunacion.
     */
    @Test
    public void testSetMes() {
        Vacunacion instance = new Vacunacion();
        int expResult = 6;
        instance.setMes(6);
        int result = instance.getMes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getVacunas method, of class Vacunacion.
     */
    @Test
    public void testGetVacunas() {
        Vacunacion instance = new Vacunacion();
        instance.setMes(8);
        instance.setFechaAdministradas(new Date());
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("Polio"));
        vacunas.add(new Vacuna("BCG"));
        vacunas.add(new Vacuna("Triplesuperbacteriana"));
        
        instance.setVacunas(vacunas);
        
        assertEquals(3, instance.getVacunas().size());
    }

    /**
     * Test of setVacunas method, of class Vacunacion.
     */
    @Test
    public void testSetVacunas() {
        Vacunacion instance = new Vacunacion();
        instance.setMes(8);
        instance.setFechaAdministradas(new Date());
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("Polio"));
        vacunas.add(new Vacuna("BCG"));
        vacunas.add(new Vacuna("Triplesuperbacteriana"));
        
        instance.setVacunas(vacunas);
        
        assertEquals(3, instance.getVacunas().size());
    }

    /**
     * Test of equals method, of class Vacunacion.
     */
    @Test
    public void testEquals() {
        
        Date unaFecha = new Date();
        
        Vacunacion instance = new Vacunacion();
        instance.setMes(8);
        instance.setFechaAdministradas(unaFecha);
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("Polio"));
        vacunas.add(new Vacuna("BCG"));
        vacunas.add(new Vacuna("Triplesuperbacteriana"));
        
        instance.setVacunas(vacunas);
        
        
        Vacunacion instance2 = new Vacunacion(8, unaFecha, vacunas);
        boolean expResult = true;
        boolean result = instance.equals(instance2);
        assertEquals(expResult, result);
    }

    /**
     * Test of compareTo method, of class Vacunacion.
     */
    @Test
    public void testCompareTo() {
        
        
        Date unaFecha = new Date();
        
        Vacunacion instance = new Vacunacion();
        instance.setMes(8);
        instance.setFechaAdministradas(unaFecha);
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("Polio"));
        vacunas.add(new Vacuna("BCG"));
        vacunas.add(new Vacuna("Triplesuperbacteriana"));
        
        instance.setVacunas(vacunas);
        
        Vacunacion instance2 = new Vacunacion(8, unaFecha, vacunas);
        
        int expResult = 0;
        int result = instance.compareTo(instance2);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Vacunacion.
     */
    @Test
    public void testToString() {
        Vacunacion instance = new Vacunacion();
        
        instance.setMes(8);
        instance.setFechaAdministradas(new Date());
        
        ArrayList<Vacuna> vacunas = new ArrayList<Vacuna>();
        vacunas.add(new Vacuna("Polio"));
        vacunas.add(new Vacuna("BCG"));
        vacunas.add(new Vacuna("Triplesuperbacteriana"));
        
        instance.setVacunas(vacunas);
        
        String expResult = "8-[Polio, BCG, Triplesuperbacteriana]";
        String result = instance.toString();
        
        assertEquals(expResult, result);
    }
    
}
