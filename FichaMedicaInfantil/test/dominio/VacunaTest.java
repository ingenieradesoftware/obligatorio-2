package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Clase VacunaTest, pruebas unitarias.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class VacunaTest {
    
    public VacunaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testConstructorSinParametros() {
        Vacuna v = new Vacuna();
        
        Vacuna v1 = new Vacuna();
        
        assertEquals("", v.getNombre());
        assertEquals("", v.toString());
        assertEquals(v, v1);
    }
    
    @Test
    public void testConstructorConParametros() {
        Vacuna v = new Vacuna("Triple viral (SRP)");
        
        Vacuna v1 = new Vacuna("Triple viral (SRP)");
        
        assertEquals("Triple viral (SRP)", v.getNombre());
        assertEquals("Triple viral (SRP)", v.toString());
        assertEquals(v, v1);
    }
    
    /**
     * Test of getNombre method, of class Vacuna.
     */
    @Test
    public void testGetNombreVacio() {
        Vacuna instance = new Vacuna();
        String expResult = "";
        instance.setNombre("");
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNombre method, of class Vacuna.
     */
    @Test
    public void testGetNombreNoVacio() {
        Vacuna instance = new Vacuna();
        String expResult = "BCG";
        instance.setNombre("BCG");
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setNombre method, of class Vacuna.
     */
    @Test
    public void testSetNombre() {
        String nombre = "Neumococo13";
        Vacuna v = new Vacuna();
        v.setNombre(nombre);
        
        assertEquals(nombre, v.getNombre());
    }

    /**
     * Test of equals method, of class Vacuna.
     */
    @Test
    public void testEquals() {
        Vacuna instance1= new Vacuna();
        Vacuna instance= new Vacuna();
        
        instance.setNombre("Hepatitis C");
        instance1.setNombre("Hepatitis C");
        
        boolean result = instance.equals(instance1);
        
        assertTrue(result);
    }

    /**
     * Test of compareTo method, of class Vacuna.
     */
    @Test
    public void testCompareToIguales() {
        Vacuna unaVacuna = new Vacuna();
        Vacuna v = new Vacuna();
        int result = v.compareTo(unaVacuna);
        assertEquals(0, result);
    }

    @Test
    public void testCompareToIzqMenor() {
        Vacuna unaVacuna = new Vacuna();
        Vacuna v = new Vacuna();
        unaVacuna.setNombre("Poliomielitica Inactivada");
        int result = v.compareTo(unaVacuna);
        
        assertTrue(result<0);
    }
    
    @Test
    public void testCompareToDerMayor() {
        Vacuna unaVacuna = new Vacuna();
        Vacuna v = new Vacuna("Poliomielitica Inactivada");
        int result = v.compareTo(unaVacuna);
        
        assertTrue(result>0);
    }
    
    /**
     * Test of toString method, of class Vacuna.
     */
    @Test
    public void testToStringSinParametros() {
        Vacuna v = new Vacuna();
        String result = v.toString();
        assertEquals("", result);
    }
    
    @Test
    public void testToStringConParametros() {
        Vacuna v = new Vacuna("Varicela");
        String result = v.toString();
        assertEquals("Varicela", result);
    }
}
