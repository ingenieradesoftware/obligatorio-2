package fichamedicainfantil;

import dominio.Consulta;
import dominio.EsquemaVacuna;
import dominio.Ninio;
import dominio.Vacuna;
import dominio.Vacunacion;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import persistencia.ArchivoLectura;

/**
* Clase que prepara el sistema para su uso.
* 
* @author Pablo Benitez (179924) y Luis Introini (181108)
* */
public class Sistema implements Serializable {
    
    /**
     * Lista de niños en el sistema.
     */
    private ArrayList<Ninio> listaninios;
    
    /**
     * Lista de Esquemas de vacunación.
     */
    private ArrayList<EsquemaVacuna> listaesquemas;

    public ArrayList<Ninio> getListaninios() {
        return listaninios;
    }

    public ArrayList<EsquemaVacuna> getListaesquemas() {
        return listaesquemas;
    }

    public void setListaninios(ArrayList<Ninio> listaninios) {
        this.listaninios = listaninios;
    }

    public void setListaesquemas(ArrayList<EsquemaVacuna> listaesquemas) {
        this.listaesquemas = listaesquemas;
    }  
    
    public Sistema() {
        listaninios   = new ArrayList<Ninio>();
        listaesquemas = new ArrayList<EsquemaVacuna>();
        
        inicializarEsquemaVacunacion();
    }
    
    public void agregarNinio (Ninio n){
        listaninios.add(n);
    }
    
    public boolean agregarNinio (String nombres, String apellidos, Date fechaNac, String cedula) {
        
        Ninio n = new Ninio();
        n.setNombres(nombres);
        n.setApellidos(apellidos);
        n.setFechaNacimiento(fechaNac);
        n.setCedula(cedula);
        n.setEsquemaVacunacion(this.getListaesquemas().get(0));
        
        if(!this.existeNinio(nombres, apellidos))
            return listaninios.add(n);
        else
            return false;
    }
    
    public boolean eliminarNinio(String nombres, String apellidos) {
        Ninio n = new Ninio();
        n.setNombres(nombres);
        n.setApellidos(apellidos);
        
        if(this.existeNinio(nombres, apellidos))
            return listaninios.remove(n);
        else
            return false;        
    }
    
    private boolean existeNinio(String nombres, String apellidos) {
        Ninio n = new Ninio();
        n.setNombres(nombres);
        n.setApellidos(apellidos);
        
        return this.listaninios.contains(n);
    }
    
    public boolean eliminarNinio(String nomApe) {
        String nombres   = this.getComponenteCadenaSeparadaPor(nomApe, "  ", 1);
        String apellidos = this.getComponenteCadenaSeparadaPor(nomApe, "  ", 2);
        
        return this.eliminarNinio(nombres, apellidos);
    }
    
    /**
     *
     * 
     * @param nomApe
     * @return 
     */
    public Ninio getNinioPorNomYApe(String nomApe) {
        
        /*nombre + "  " + apellido*/
    
        String nombres   = this.getComponenteCadenaSeparadaPor(nomApe, "  ", 1);
        String apellidos = this.getComponenteCadenaSeparadaPor(nomApe, "  ", 2);
        
        Ninio filtro = new Ninio(nombres, apellidos, new Date(0L), "", new ArrayList<Consulta>(), new EsquemaVacuna());
        
        int posicionEnLista = this.listaninios.indexOf(filtro);
        
        if(posicionEnLista != -1) {
            return this.listaninios.get(posicionEnLista);
        }
        
        return new Ninio();
    }
    
    /**
     * 
     * @param unaCadena
     * @param separador
     * @param nroComponente
     * @return 
     */
    protected String getComponenteCadenaSeparadaPor(String unaCadena, String separador, int nroComponente) {
        
        String[] split = unaCadena.split(separador);
        
        int splitLength = split.length;
        
        if(nroComponente == 1) {
            return split[0];
        }
        else if(nroComponente == 2) {
            if(splitLength >= 2)
                return split[1];
        }
        
        return "";
    }

    /**
     * Inicializa el sistema con datos.
     */
    private void inicializarEsquemaVacunacion() {
        
        //Cargar esquema de Vacunación.
        java.net.URL pathToDefaultEsquema = this.getClass().getResource("/datos/EsquemaVacunas.txt");
        
        ArchivoLectura configuracion = new ArchivoLectura(pathToDefaultEsquema.getPath());
        
        configuracion.hayMasLineas();
        String nombreEsquema = configuracion.linea();
        
        configuracion.hayMasLineas();
        String[] fecha   = configuracion.linea().split("-");
        Date fechaCreado = new Date(Integer.parseInt(fecha[2])-1900, Integer.parseInt(fecha[1]), Integer.parseInt(fecha[0]));
        
        EsquemaVacuna esquema = new EsquemaVacuna(nombreEsquema, fechaCreado, new ArrayList<Vacunacion>());
        
        int mes;
        while (configuracion.hayMasLineas()) {
            String linea = configuracion.linea();
            
            String[] lineas = linea.split("~");

            String nombreVacuna = lineas[0];

            String[] aus = lineas[1].split(",");

            for (int i = 0; i < aus.length; i++) {
                mes = Integer.parseInt(aus[i]);
                Vacunacion v = esquema.getVacunacionParaMes(mes);
                
                if(v.getMes() == mes) { /* Ya existe Vacunación. */
                    v.getVacunas().add(new Vacuna(nombreVacuna));
                }
                else {
                    v.setMes(mes);
                    v.getVacunas().add(new Vacuna(nombreVacuna));
                    esquema.getVacunaciones().add(v);
                }                
                
            }

        }
        
        
        this.getListaesquemas().add(esquema);
        
    }    
    
    public ArrayList<Consulta> getTodasLasConsultasProxSieteDias(Ninio unNinio) {
        
        ArrayList<Consulta> consultas = new ArrayList<Consulta>();
        
        LocalDateTime ahora   = LocalDateTime.now();
        LocalDateTime maxDate = ahora.plusDays(7);
        
        Iterator<Consulta> iterC = unNinio.getConsultas().iterator();
        while (iterC.hasNext()) {
            Consulta c = iterC.next();
            
            if (!c.isAsistencia()) {
                if (c.getFecha().isAfter(ahora)) {
                    if (c.getFecha().isBefore(maxDate)) {
                        consultas.add(c);
                    }
                }
            }

        }
            
        return consultas;
    }
    
}
