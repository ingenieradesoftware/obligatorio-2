package persistencia;

import java.io.*;

/**
 *
 * @author Pablo
 */
public class ObjetoDeserializar {

    FileInputStream fis;

    BufferedInputStream bis;

    ObjectInputStream ois;

    public ObjetoDeserializar(String unNombreArchivo) {

        try {

            fis = new FileInputStream(unNombreArchivo);
            bis = new BufferedInputStream(fis);
            ois = new ObjectInputStream(bis);

        }
        catch (FileNotFoundException ex) {

        }
        catch (IOException ex) {

        }

    }

    public Object deserializar() {

        Object deserializado = null;

        try {

            if(ois != null) {
                deserializado = ois.readObject();
            }

        }
        catch (IOException | ClassNotFoundException ex) {

        }

        return deserializado;
    }
}
