package persistencia;

import java.io.*;

/**
 *
 * @author Cátedra Programación Universidad ORT Uruguay
 */
public class ArchivoGrabacion {

    BufferedWriter out;

    public ArchivoGrabacion(String unNombre) {

        try {
            out = new BufferedWriter(new FileWriter(unNombre));
        }
        catch(IOException e) {
            //TODO
        }

    }

    public boolean grabarLinea(String unaLinea) {

        boolean ok = true;

        try {
            out.write(unaLinea);
            out.newLine();
        }
        catch(IOException e) {
            ok = false;
        }

        return ok;
    }

    public boolean cerrar() {

        boolean ok = true;

        try {
            out.flush();
            out.close();
        }
        catch(Exception e) {
            ok = false;
        }

        return ok;

    }

}
