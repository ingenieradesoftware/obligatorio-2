package persistencia;

import java.io.*;

/**
 *
 * @author Pablo
 */
public class ObjetoSerializar {

    FileOutputStream fos;

    BufferedOutputStream bos;

    ObjectOutputStream oos;

    public ObjetoSerializar(String unNombreArchivo) {

        try {
            fos = new FileOutputStream(unNombreArchivo);
            bos = new BufferedOutputStream(fos);
            oos = new ObjectOutputStream(bos);
        }
        catch(FileNotFoundException ex) {

        }
        catch(IOException ex) {

        }

    }

    public void serializar(Object unObjeto) {

        try {
            oos.writeObject(unObjeto);
            oos.close();
        }
        catch (IOException ex) {

        }

    }
}
