package dominio;

import java.io.Serializable;

/**
 * Clase Vacuna.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class Vacuna implements Comparable <Vacuna>, Serializable {
    
    /**
     * Nombre de la Vacuna.
     */
    private String nombre;

    /**
     * Constructor sin parámetros.
     */
    public Vacuna() {
        this.nombre = "";
    }
    
    /**
     * Constructor con parametros.
     * 
     * @param unNombre 
     */
    public Vacuna(String unNombre) {
        this.nombre = unNombre;
    }
    
    /**
     * Devuelve el nombre de una Vacuna.
     * @return 
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Guarda el nombre de una Vacuna.
     * 
     * @param unNombre 
     */
    public void setNombre(String unNombre) {
        this.nombre = unNombre;
    }
    
    /**
     * Equals de la clase Vacuna, chequea por nombre.
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        Vacuna other = (Vacuna) obj;

        return this.getNombre().equalsIgnoreCase(other.getNombre());
    }
    
    /**
     * Ordena lista de vacunas por nombre de forma ascendente.
     *
     * @param unaVacuna
     * @return
     */
    @Override
    public int compareTo(Vacuna unaVacuna) {
        return this.getNombre().compareTo(unaVacuna.getNombre());
    }

    /**
     * Impresión de Vacuna.
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.getNombre();
    }
}