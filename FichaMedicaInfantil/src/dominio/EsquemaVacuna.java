package dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Clase EsquemaVacuna.
 *
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class EsquemaVacuna implements Comparable<EsquemaVacuna>, Serializable {
    
    /**
     * Nombre del esquema de vacunación.
     */
    private String nombre;
    
    /**
     * Fecha de creado.
     */
    private Date fechaCreado;
    
    /**
     * Vacunas del esquema.
     */
    private ArrayList<Vacunacion> vacunacion;
    
    /**
     * Constructor sin parámetros.
     */
    public EsquemaVacuna() {
        this.nombre      = "";
        this.fechaCreado = new Date(0L);
        this.vacunacion  = new ArrayList<Vacunacion>();
    }
    
    /**
     * Constructor con parámetros.
     * 
     * @param unNombre
     * @param unaFecha 
     * @param vacunacion
     */
    public EsquemaVacuna(String unNombre, Date unaFecha, ArrayList<Vacunacion> vacunacion) {
        this.nombre      = unNombre;
        this.fechaCreado = unaFecha;
        this.vacunacion  = vacunacion;
    }

    /**
     * Devuelve el nombre de este esquema.
     * 
     * @return Nombre del esquema.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Guarda el nombre del Esquema.
     * 
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Devuelve la fecha en la que se creó el EsquemaVacuna.
     * 
     * @return La fecha de creado del esquema.
     */
    public Date getFechaCreado() {
        return fechaCreado;
    }

    /**
     * Guarda la fecha de creado del EsquemaVacuna.
     * 
     * @param fechaCreado
     */
    public void setFechaCreado(Date fechaCreado) {
        this.fechaCreado = fechaCreado;
    }
    
    /**
     * Devuelve las vacunas del esquema.
     * 
     * @return
     */
    public ArrayList<Vacunacion> getVacunaciones() {
        return vacunacion;
    }

    /**
     * Guarda las vacunas en el esquema.
     * 
     * @param vacunacion
     */
    public void setVacunaciones(ArrayList<Vacunacion> vacunacion) {
        this.vacunacion = vacunacion;
    }
    
    public Vacunacion getVacunacionParaMes(int unMes) {
        Vacunacion cfiltro = new Vacunacion();
        cfiltro.setMes(unMes);
        
        if (this.getVacunaciones().contains(cfiltro))
            return this.getVacunaciones().get(this.getVacunaciones().indexOf(cfiltro));
        
        return new Vacunacion();
    }
    
    /**
     * Ordena lista de esquemas por nombre de forma ascendente.
     *
     * @param unEsquema
     * @return
     */
    @Override
    public int compareTo(EsquemaVacuna unEsquema) {
        return this.getNombre().compareTo(unEsquema.getNombre());
    }
    
    /**
     * Equals de la clase EsquemaVacuna, chequea por nombre.
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        EsquemaVacuna other = (EsquemaVacuna) obj;

        return this.getNombre().equals(other.getNombre());
    }
    
    /**
     * Impresión de EsquemaVacuna.
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.getNombre() + " (" + this.getFechaCreado() + ")";
    }
}
