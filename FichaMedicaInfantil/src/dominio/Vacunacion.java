package dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Clase Vacunacion.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class Vacunacion implements Comparable <Vacunacion>, Serializable {
    
    /**
     * Mes de la vacunación.
     */
    private int mes;
    
    /**
     * Fecha de administradas las vacunas.
     */
    private Date fechaAdministradas;
    
    /**
     * Vacunas de la vacunación.
     */
    private ArrayList<Vacuna> vacunas;

    /**
     * Guarda la fecha de administrada de las vacunas.
     * 
     * @param unaFecha 
     */
    public void setFechaAdministradas(Date unaFecha) {
        this.fechaAdministradas = unaFecha;
    }

    /**
     * Devuelve la fecha de administrada de las vacunas.
     * 
     * @return 
     */
    public Date getFechaAdministradas() {
        return fechaAdministradas;
    }
    
    /**
     * Devuelve el mes de la vacunación.
     * 
     * @return 
     */
    public int getMes() {
        return mes;
    }

    /**
     * Guarda el mes de la vacunación.
     * 
     * @param unMes 
     */
    public void setMes(int unMes) {
        this.mes = unMes;
    }

    /**
     * @return the vacunas
     */
    public ArrayList<Vacuna> getVacunas() {
        return vacunas;
    }

    /**
     * @param vacunas the vacunas to set
     */
    public void setVacunas(ArrayList<Vacuna> vacunas) {
        this.vacunas = vacunas;
    }
    
    /**
     * Constructor sin parámetros.
     */
    public Vacunacion() {
        this.mes                = -1;
        this.fechaAdministradas = new Date(0L);
        this.vacunas            = new ArrayList<Vacuna>();
    }
    
    /**
     * Constructor con parámetros.
     * 
     * @param unMes
     * @param unaFecha
     * @param unasVacunas 
     */
    public Vacunacion(int unMes, Date unaFecha, ArrayList<Vacuna> unasVacunas) {
        this.mes                = unMes;
        this.fechaAdministradas = unaFecha;
        this.vacunas            = unasVacunas;
    }
    
    /**
     * Equals de la clase Vacunacion, chequea por nombre.
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        Vacunacion other = (Vacunacion) obj;        
        
        return (this.getMes() == other.getMes());
    }

    /**
     * Ordena lista de vacunas por nombre de forma ascendente.
     *
     * @param unaVacunacion
     * @return
     */
    @Override
    public int compareTo(Vacunacion unaVacunacion) {
        return Integer.compare(this.getMes(), unaVacunacion.getMes());
    }

    /**
     * Impresión de Vacunacion.
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.getMes() + "-" + this.getVacunas().toString();
    }
    
}