package dominio;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Clase Consulta.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class Consulta implements Comparable <Consulta>, Serializable {
    
    /**
     * Fecha de la consulta.
     */
    private LocalDateTime fecha;
    
    /**
     * Notas obtenidas al realizar la consulta.
     */
    private String nota;
    
    /**
     * Asistio o no.
     */
    private boolean asistencia;
    
    /**
     * Peso del paciente en kilogramos.
     */
    private float peso;
    
    /**
     * Altura en centimetros.
     */
    private int talla;
    
    /**
     * Perimetro craneal en centimetros.
     */
    private float perimetroCraneal;

    /**
     * Edad del paciente en meses.
     */
    private int edad;
    
    /**
     * Constructor sin parámetros.
     */
    public Consulta() {
        this.talla            = 0;
        this.asistencia       = false;
        this.fecha            = LocalDateTime.MIN;
        this.nota             = "";
        this.perimetroCraneal = 0;
        this.peso             = 0;
        this.edad             = 0;
    }
    
    /**
     * Constructor con parámetros.
     * 
     * @param unaFecha
     * @param unaNota
     * @param asistio
     * @param unPeso
     * @param unaAltura
     * @param unPerimetro
     * @param unaEdad
     */
    public Consulta(LocalDateTime unaFecha, String unaNota, boolean asistio, float unPeso, int unaAltura, float unPerimetro, int unaEdad) {
        this.talla            = unaAltura;
        this.asistencia       = asistio;
        this.fecha            = unaFecha;
        this.nota             = unaNota;
        this.perimetroCraneal = unPerimetro;
        this.peso             = unPeso;
        this.edad             = unaEdad;
    }
    
    /**
     * Devuelve la fecha de la consulta.
     * 
     * @return 
     */
    public LocalDateTime getFecha() {
        return fecha;
    }

    /**
     * Devuelve las notas correspondientes a la consulta.
     * 
     * @return 
     */
    public String getNota() {
        return nota;
    }

    /**
     * Devuelve si hubo asistencia a la consulta o no.
     * 
     * @return 
     */
    public boolean isAsistencia() {
        return asistencia;
    }

    /**
     * Devuelve la edad del paciente al momento de hacer la consulta.
     * 
     * @return 
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Devuelve el peso del niño registrado en la consulta.
     * 
     * @return 
     */
    public float getPeso() {
        return peso;
    }

    /**
     * Devuelve la talla (altura) del paciente registrado en la consulta.
     * 
     * @return 
     */
    public int getTalla() {
        return talla;
    }

    /**
     * Devuelve el perímetro craneal registrado en la consulta.
     * 
     * @return 
     */
    public float getPerimetroCraneal() {
        return perimetroCraneal;
    }

    /**
     * Guarda la fecha en la que fué realizada la consulta.
     * 
     * @param fecha 
     */
    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    /**
     * Guarda las notas correspondientes a la consulta.
     * 
     * @param nota 
     */
    public void setNota(String nota) {
        this.nota = nota;
    }

    /**
     * Registra si hubo asistencia a la consulta.
     * 
     * @param asistencia 
     */
    public void setAsistencia(boolean asistencia) {
        this.asistencia = asistencia;
    }

    /**
     * Guarda la edad del paciente al momento de la consulta.
     * 
     * @param edad 
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * Guarda el peso en grams del paciente al momento de la consulta.
     * 
     * @param peso 
     */
    public void setPeso(float peso) {
        this.peso = peso;
    }

    /**
     * Guarda la altura en centímetros del paciente al momento de la consulta.
     * 
     * @param talla 
     */
    public void setTalla(int talla) {
        this.talla = talla;
    }

    /**
     * Guarda el perímetro craneal del paciente al momento de la consulta.
     * 
     * @param perimetroCraneal 
     */    
    public void setPerimetroCraneal(float perimetroCraneal) {
        this.perimetroCraneal = perimetroCraneal;
    }

    /**
     * Ordena lista de consultas por fecha de forma ascendente.
     *
     * @param unaConsulta
     * @return
     */
    @Override
    public int compareTo(Consulta unaConsulta) {
        return this.getFecha().compareTo(unaConsulta.getFecha());
    }
    
    /**
     * Equals de la clase Vacuna, chequea por nombre.
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        Consulta other = (Consulta) obj;

        DateTimeFormatter d = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        
        return this.getFecha().format(d).equals(other.getFecha().format(d));
    }
    
    /**
     * Impresión de Consulta.
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.getFecha().toString();
    }
}
