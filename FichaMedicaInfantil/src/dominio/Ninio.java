package dominio;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

/**
 * Clase Ninio.
 *
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class Ninio implements Comparable<Ninio>, Serializable {

    /**
     * Nombres del niño.
     */
    private String nombres;

    /**
     * Apellidos del niño.
     */
    private String apellidos;

    /**
     * Fecha de nacimiento del niño.
     */
    private Date fechaDeNacimiento;

    /**
     * Cédula del niño.
     */
    private String cedula;

    /**
     * Consultas del niño.
     */
    private ArrayList<Consulta> consultas;

    /**
     * Esquema de vacunación del Niño.
     */
    private EsquemaVacuna esquemaVacunacion;

    /**
     * Constructor sin parámetros.
     */
    public Ninio() {
        this.nombres = "";
        this.apellidos = "";
        this.fechaDeNacimiento = new Date(0L);
        this.cedula = "";
        this.consultas = new ArrayList<Consulta>();
        this.esquemaVacunacion = new EsquemaVacuna();
    }

    /**
     * Constructor con parámetros.
     *
     * @param nombres
     * @param apellidos
     * @param fechaDeNacimiento
     * @param cedula
     * @param consultas
     * @param esquemavacunas
     */
    public Ninio(String nombres, String apellidos, Date fechaDeNacimiento, String cedula, ArrayList<Consulta> consultas, EsquemaVacuna esquemavacunas) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.cedula = cedula;
        this.consultas = consultas;
        this.esquemaVacunacion = esquemavacunas;
    }

    /**
     * Devuelve los nombres de un Ninio.
     *
     * @return Los nombres del niño.
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * Devuelve los apellidos de un Ninio.
     *
     * @return Los apellidos del niño.
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Devuelve la fecha de nacimiento del Ninio.
     *
     * @return La fecha de nacimiento.
     */
    public Date getFechaNacimiento() {
        return fechaDeNacimiento;
    }

    /**
     * Guarda los nombres del Ninio.
     *
     * @param nombres
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * Guarda los apellidos del Ninio.
     *
     * @param apellidos
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Guarda la fecha de nacimiento del Ninio.
     *
     * @param fechaDeNacimiento
     */
    public void setFechaNacimiento(Date fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    /**
     * Guarda la cédula del Ninio.
     *
     * @param unaCedula
     */
    public void setCedula(String unaCedula) {
        this.cedula = unaCedula;
    }

    /**
     * Devuelve el nro de cédula del Ninio.
     *
     * @return El nro de cédula.
     */
    public String getCedula() {
        return this.cedula;
    }

    /**
     * Devuelve las consultas de un Niño.
     *
     * @return
     */
    public ArrayList<Consulta> getConsultas() {
        return consultas;
    }

    /**
     * Guarda las consultas de un Niño.
     *
     * @param consultas
     */
    public void setConsultas(ArrayList<Consulta> consultas) {
        this.consultas = consultas;
    }

    /**
     * Devuelve el esquema vacuna del Niño.
     *
     * @return the esquemaVacunacion
     */
    public EsquemaVacuna getEsquemaVacunacion() {
        return esquemaVacunacion;
    }

    /**
     * Guarda el esquema de vacunación para el Niño.
     *
     * @param esquemaVacunacion the esquemaVacunacion to set
     */
    public void setEsquemaVacunacion(EsquemaVacuna esquemaVacunacion) {
        this.esquemaVacunacion = esquemaVacunacion;
    }

    /**
     * Agrega una Consulta a las consultas del niño.
     *
     * @param unaConsulta
     */
    public void addConsulta(Consulta unaConsulta) {
        this.getConsultas().add(unaConsulta);
    }

    /**
     * Devuelve una lista que contiene las consultas que son anteriores a hoy.
     *
     * @return
     */
    public ArrayList<Consulta> getConsultasPasadas() {
        return this.getConsultasFiltroFecha(true);
    }

    /**
     * Devuelve una lista que contiene las consultas que son posteriores a hoy.
     *
     * @return
     */
    public ArrayList<Consulta> getConsultasFuturas() {
        return this.getConsultasFiltroFecha(false);
    }

    /**
     * Devuelve consultas anteriores o posteriores a hoy. Si el parámetro
     * beforeToday viene en false se devuelve las anteriores a hoy, si viene en
     * true solamente las posteriores a hoy.
     *
     * @param beforeToday
     * @return
     */
    private ArrayList<Consulta> getConsultasFiltroFecha(boolean beforeToday) {
        boolean cumpleCondicion;

        ArrayList<Consulta> consultasFiltradas = new ArrayList<Consulta>();

        LocalDateTime ahora = LocalDateTime.now();

        ArrayList<Consulta> todasLasConsultas = this.getConsultas();
        Iterator<Consulta> iter = todasLasConsultas.iterator();
        while (iter.hasNext()) {
            cumpleCondicion = false;
            Consulta consulta = iter.next();

            if (beforeToday) {
                if (consulta.getFecha().isBefore(ahora)) {
                    cumpleCondicion = true;
                }
            } else if (consulta.getFecha().isAfter(ahora)) {
                cumpleCondicion = true;
            }

            if (cumpleCondicion) {
                consultasFiltradas.add(consulta);
            }

        }

        return consultasFiltradas;
    }

    /**
     * Devuelve todas las vacunas del Ninio (Esquema).
     *
     * @return
     */
    public ArrayList<Vacuna> getTodasLasVacunas() {

        ArrayList<Vacuna> lasVacunas = new ArrayList<Vacuna>();

        ArrayList<Vacunacion> vacunaciones = this.getEsquemaVacunacion().getVacunaciones();

        Iterator<Vacunacion> iter = vacunaciones.iterator();
        while (iter.hasNext()) {
            Vacunacion vacncion = iter.next();
            lasVacunas.addAll(vacncion.getVacunas());
        }

        return lasVacunas;

    }

    /**
     * Devuelve una consulta por Fecha.
     *
     * @param unaFecha
     * @return
     */
    public Consulta getConsultaParaFecha(LocalDateTime unaFecha) {
        Consulta cfiltro = new Consulta();
        cfiltro.setFecha(unaFecha);

        if (this.getConsultas().contains(cfiltro)) {
            return this.getConsultas().get(this.getConsultas().indexOf(cfiltro));
        }

        return new Consulta();
    }

    /**
     * Actualiza una consulta.
     *
     * @param unaConsulta
     */
    public void actualizarConsulta(Consulta unaConsulta) {
        Consulta c = this.getConsultaParaFecha(unaConsulta.getFecha());

        if (c.equals(unaConsulta)) {
            c.setAsistencia(unaConsulta.isAsistencia());
            c.setEdad(unaConsulta.getEdad());
            c.setNota(unaConsulta.getNota());
            c.setPerimetroCraneal(unaConsulta.getPerimetroCraneal());
            c.setPeso(unaConsulta.getPeso());
            c.setTalla(unaConsulta.getTalla());
        }

    }

    public Vacunacion getVacunacionPorMes(int mes) {
        Iterator<Vacunacion> i = this.getEsquemaVacunacion().getVacunaciones().iterator();
        Date d = new Date("01/01/0001");
        Vacunacion vacFiltro = new Vacunacion(mes, d, new ArrayList<Vacuna>());
        while (i.hasNext()) {
            Vacunacion aux = i.next();
            if (aux.equals(vacFiltro)) {
                return aux;
            }
        }
        return new Vacunacion();
    }

    public void administrarVacuna(Vacunacion unaVacunacion) {
        Iterator<Vacunacion> i = this.getEsquemaVacunacion().getVacunaciones().iterator();
        while (i.hasNext()) {
            Vacunacion aux = i.next();
            if (aux.equals(unaVacunacion)) {
                aux.setFechaAdministradas(unaVacunacion.getFechaAdministradas());
            }
        }
    }

    /**
     * Equals de la clase Ninio, chequea por nombre.
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        Ninio other = (Ninio) obj;

        return (this.getNombres().equalsIgnoreCase(other.getNombres())
                && this.getApellidos().equalsIgnoreCase(other.getApellidos()));
    }

    /**
     * Devuelve la edad en meses del Niño.
     *
     * @return
     */
    public int getEdadEnMeses() {
        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(this.getFechaNacimiento());
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(new Date());

        int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        
        int edad = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

        return edad;
    }

    /**
     * Ordena lista de niños por nombres de forma ascendente.
     *
     * @param unNinio
     * @return
     */
    @Override
    public int compareTo(Ninio unNinio) {
        return this.getNombres().compareTo(unNinio.getNombres());
    }

    /**
     * Impresión de Ninio.
     *
     * @return String
     */
    @Override
    public String toString() {
        String laCedula = "";

        if (!this.getCedula().isEmpty()) {
            laCedula = " " + "(" + this.getCedula() + ")";
        }

        return (this.getNombres() + " " + this.getApellidos() + laCedula).trim();
    }

}
