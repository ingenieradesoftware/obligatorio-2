
package Interface;

import java.awt.Component;
import javax.swing.JOptionPane;

/**
 * Clase VentanaAltaNinio para dar de alta un niño.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class Ventana {

    /**
     * Constructor sin parámetros.
     *
     */
    public Ventana() {

    }

    /**
     * Muestra un mensaje con un JOptionPane
     * 
     * @param parentComponent
     * @param message
     * @param title
     * @param messageType
     */
    public static void mensaje(Component parentComponent, String message, String title, int messageType) {
        JOptionPane.showMessageDialog(parentComponent, message, title, messageType);
    }

}