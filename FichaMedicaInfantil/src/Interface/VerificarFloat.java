package Interface;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 * Clase VentanaAltaNinio para dar de alta un niño.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class VerificarFloat extends InputVerifier {

    /**
     * Verificamos que la entrada sea un float.
     * 
     * @param input
     * @return
     */
    @Override
    public boolean verify(JComponent input) {
        JTextField tf = (JTextField) input;

        boolean okFloat = true;

        try {
            Float.parseFloat(tf.getText());
        }
        catch(NumberFormatException ex) {
            okFloat = false;
        }

        return okFloat;
    }
}
