package Interface;

import fichamedicainfantil.Sistema;
import java.awt.FlowLayout;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

//import de.javasoft.plaf.synthetica.SyntheticaBlackEyeLookAndFeel;
import javax.swing.JOptionPane;
import dominio.*;
//import fichaMedicaInfantil.*;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.JList;

/**
 * Clase Ventana Principal para dar de alta un niño.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class VentanaPrincipal extends javax.swing.JFrame {
    /**
     * Creates new form Principal
     */
    Sistema unSistema;
    
    public VentanaPrincipal(Sistema unSistemaPar) {      
        initComponents();
        this.unSistema = unSistemaPar;

        FuncionesGenerales.CargarDatosPrueba(this.unSistema);
        FuncionesGenerales.cargarComboninios(cmbNinios, unSistema.getListaninios());
        FuncionesGenerales.cargarListaConsultasProximas(lstConsultas, unSistema);
        FuncionesGenerales.cargarListaVacunasProximas(lstVacunas, unSistema);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        cmbNinios = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        lblPrincipal = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstConsultas = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        lblVacunasProx = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstVacunas = new javax.swing.JList<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuPrincipal = new javax.swing.JMenu();
        menuConsultas = new javax.swing.JMenuItem();
        menu_vacunas = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSalir = new javax.swing.JMenuItem();
        menuOpciones = new javax.swing.JMenu();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();

        jMenuItem1.setText("jMenuItem1");

        jMenuItem2.setText("jMenuItem2");

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        jMenuItem3.setText("jMenuItem3");

        jMenuItem4.setText("jMenuItem4");

        jMenuItem5.setText("jMenuItem5");

        jMenuItem6.setText("jMenuItem6");

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        jMenuItem7.setText("jMenuItem7");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Carné de salud del niño y la niña");
        setAutoRequestFocus(false);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        cmbNinios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbNiniosActionPerformed(evt);
            }
        });

        jLabel1.setText("Niño:");

        lblPrincipal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/Principal.jpg"))); // NOI18N

        lstConsultas.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "No hay consultas." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        lstConsultas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstConsultas.setVisibleRowCount(3);
        jScrollPane1.setViewportView(lstConsultas);

        jLabel2.setLabelFor(lstConsultas);
        jLabel2.setText("Consultas en los próximos 7 dias:");

        lblVacunasProx.setLabelFor(lstConsultas);
        lblVacunasProx.setText("Próximas vacunas:");

        lstVacunas.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "No hay vacunas pendientes." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        lstVacunas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstVacunas.setVisibleRowCount(3);
        jScrollPane2.setViewportView(lstVacunas);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/Eliminar.jpg"))); // NOI18N
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/Alta.jpg"))); // NOI18N
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblVacunasProx)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbNinios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPrincipal)
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblPrincipal)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbNinios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addGap(11, 11, 11)
                        .addComponent(jLabel4)
                        .addGap(26, 26, 26)
                        .addComponent(lblVacunasProx)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(75, 75, 75)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        menuPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/nino.jpg"))); // NOI18N
        menuPrincipal.setText("NIÑO");
        menuPrincipal.setMaximumSize(new java.awt.Dimension(276, 3276));

        menuConsultas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        menuConsultas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/Consulta.png"))); // NOI18N
        menuConsultas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConsultasActionPerformed(evt);
            }
        });
        menuPrincipal.add(menuConsultas);

        menu_vacunas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        menu_vacunas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/Vacunas.png"))); // NOI18N
        menu_vacunas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_vacunasActionPerformed(evt);
            }
        });
        menuPrincipal.add(menu_vacunas);
        menuPrincipal.add(jSeparator1);

        menuSalir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        menuSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/salir.jpg"))); // NOI18N
        menuSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalirActionPerformed(evt);
            }
        });
        menuPrincipal.add(menuSalir);

        jMenuBar1.add(menuPrincipal);

        menuOpciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/Grafica.png"))); // NOI18N
        menuOpciones.setText("Gráficas");
        menuOpciones.add(jSeparator3);

        jMenuItem8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/Peso.jpg"))); // NOI18N
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        menuOpciones.add(jMenuItem8);

        jMenuItem9.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/Altura.jpg"))); // NOI18N
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        menuOpciones.add(jMenuItem9);

        jMenuItem10.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/Imagenes/PC.jpg"))); // NOI18N
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        menuOpciones.add(jMenuItem10);

        jMenuBar1.add(menuOpciones);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_menuSalirActionPerformed

    private void menuConsultasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuConsultasActionPerformed
        //Me fijo si hay algín niño en el sistema
       if (unSistema.getListaninios().size() > 0) {
           VentanaConsultas consultas = new VentanaConsultas(unSistema.getNinioPorNomYApe(cmbNinios.getSelectedItem().toString()));
           consultas.setLocationRelativeTo(this);
           consultas.setVisible(true);
           consultas.setPariente(this);
           consultas.setModelo(unSistema);
       } else {
           JOptionPane.showMessageDialog(null, "Para ver consultas primero debe seleccionar un niño.", "Ficha medica infantil", 1);
       }
    }//GEN-LAST:event_menuConsultasActionPerformed

    private void menu_vacunasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_vacunasActionPerformed
        //Me fijo si hay algín niño en el sistema
        if (unSistema.getListaninios().size() > 0) {
            VentanaVacunas vacunas;
            vacunas = new VentanaVacunas(unSistema.getNinioPorNomYApe(cmbNinios.getSelectedItem().toString()));
            vacunas.setLocationRelativeTo(this);
            vacunas.setVisible(true);
            
        } else {
            JOptionPane.showMessageDialog(null, "Para ver vacunas primero debe seleccionar un niño.", "Ficha medica infantil", 1);
        }
    }//GEN-LAST:event_menu_vacunasActionPerformed

    private void cmbNiniosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbNiniosActionPerformed

    }//GEN-LAST:event_cmbNiniosActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        
        Object ninio = cmbNinios.getSelectedItem();
        if(ninio == null) {
            Ventana.mensaje(this, "Por favor, seleccion un niño.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        GraficaPeso grp = new GraficaPeso(unSistema.getNinioPorNomYApe(ninio.toString()));
        grp.setLocationRelativeTo(this);
        grp.setVisible(true);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        Object ninio = cmbNinios.getSelectedItem();
        if(ninio == null) {
            Ventana.mensaje(this, "Por favor, seleccion un niño.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }        
        GraficaTalla grp = new GraficaTalla(unSistema.getNinioPorNomYApe(ninio.toString()));
        grp.setLocationRelativeTo(this);
        grp.setVisible(true);
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        
        Object ninio = cmbNinios.getSelectedItem();
        if(ninio == null) {
            Ventana.mensaje(this, "Por favor, seleccion un niño.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }        
        GraficaPC grp = new GraficaPC(unSistema.getNinioPorNomYApe(ninio.toString()));
        grp.setLocationRelativeTo(this);
        grp.setVisible(true);
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked

        Object item = cmbNinios.getSelectedItem();
        
        if(item == null) {
            Ventana.mensaje(this, "No hay niños en el sistema.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String idNinio = item.toString();
        
        Ninio n = unSistema.getNinioPorNomYApe(idNinio);
        if(n.equals(new Ninio())) {
            Ventana.mensaje(this, "No se encontró el Niño en el sistema.", "Error", JOptionPane.ERROR_MESSAGE);
        }
        else {
            boolean eliminarOk = unSistema.eliminarNinio(idNinio);
            if(!eliminarOk)
                Ventana.mensaje(this, "No se pudo eliminar a el niño.", "Error", JOptionPane.ERROR_MESSAGE);
            else {
                FuncionesGenerales.cargarComboninios(cmbNinios, unSistema.getListaninios());
                Ventana.mensaje(this, "Niño eliminado con éxito.", "Éxito", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        
        FuncionesGenerales.cargarListaConsultasProximas(this.getListaDeConsultas(), this.unSistema);
    }//GEN-LAST:event_jLabel3MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        VentanaAltaNinio ventanaNinio = new VentanaAltaNinio(unSistema);
        ventanaNinio.setLocationRelativeTo(this);
        ventanaNinio.setVisible(true);
        ventanaNinio.pariente = this;
    }//GEN-LAST:event_jLabel4MouseClicked

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        FuncionesGenerales.cargarComboninios(cmbNinios, unSistema.getListaninios());
        FuncionesGenerales.cargarListaConsultasProximas(lstConsultas, unSistema);
        FuncionesGenerales.cargarListaVacunasProximas(lstVacunas, unSistema);
    }//GEN-LAST:event_formWindowActivated

    public JComboBox getComboNinios() {
        return this.cmbNinios;
    }

    public JList<String> getListaDeConsultas() {
        return this.lstConsultas;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmbNinios;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JLabel lblPrincipal;
    private javax.swing.JLabel lblVacunasProx;
    private javax.swing.JList<String> lstConsultas;
    private javax.swing.JList<String> lstVacunas;
    private javax.swing.JMenuItem menuConsultas;
    private javax.swing.JMenu menuOpciones;
    private javax.swing.JMenu menuPrincipal;
    private javax.swing.JMenuItem menuSalir;
    private javax.swing.JMenuItem menu_vacunas;
    // End of variables declaration//GEN-END:variables
}
