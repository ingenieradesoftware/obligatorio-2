package Interface;

import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JComboBox;
import dominio.*;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import fichamedicainfantil.*;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import javax.swing.JList;
import javax.swing.JPanel;

/**
 * Clase auxiliar para interfaz.
 *
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 *
 */
public class FuncionesGenerales {

    public static void cargarComboninios(JComboBox unCombo, ArrayList<Ninio> unaLista) {
        unCombo.removeAllItems();
        Iterator<Ninio> iter = unaLista.iterator();
        while (iter.hasNext()) {
            Ninio ninio = iter.next();
            unCombo.addItem(ninio.getNombres() + "  " + ninio.getApellidos());
        }
    }

    public static void cargarTablaConsultas(JTable unaTabla, ArrayList<Consulta> consultas) {
        //unaTabla.removeAll();
        DefaultTableModel model = (DefaultTableModel) unaTabla.getModel();
        model.getDataVector().removeAllElements();
        
        Iterator<Consulta> i = consultas.iterator();
        while (i.hasNext()) {
            Consulta c = i.next();
            String l = c.getFecha().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
            Object[] o = new Object[]{l, c.isAsistencia()};
            model.insertRow(model.getRowCount(), o);
        }
    }

    public static void cargarTablaVacunas(JTable unaTabla, ArrayList<Vacunacion> vacunaciones) {
        DefaultTableModel model = (DefaultTableModel) unaTabla.getModel();
        model.getDataVector().removeAllElements();
        
        Vacunacion vaux = new Vacunacion();
        
        Iterator<Vacunacion> i = vacunaciones.iterator();
        while (i.hasNext()) {
            Vacunacion v = i.next();
            Date d = new Date("01/01/1900");
            boolean asistida = false;
            if(!v.getFechaAdministradas().equals(vaux.getFechaAdministradas())) {
                asistida = true;
            }
            Object[] o = new Object[]{v.getMes(), asistida};
            model.insertRow(model.getRowCount(), o);
        }
    }

    public static LocalDateTime GetFechaTabla(JTable jtConsultas) {
        DefaultTableModel model = (DefaultTableModel) jtConsultas.getModel();
        LocalDateTime fecha = LocalDateTime.parse(model.getValueAt(jtConsultas.getSelectedRow(), 0).toString(), DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        return fecha;
    }

    public static int GetMesTabla(JTable jtVacunas) {
        DefaultTableModel model = (DefaultTableModel) jtVacunas.getModel();
        int mes = (int) model.getValueAt(jtVacunas.getSelectedRow(), 0);
        return mes;
    }

    public static Sistema CargarDatosPrueba(Sistema unSistema) {
        Date fnac = new Date("7/9/2014");
        Date fnac1 = new Date("9/3/2015");
        LocalDateTime fcon = LocalDateTime.of(2014, Month.NOVEMBER, 8, 10, 21);
        ArrayList<Consulta> consultas = new ArrayList<Consulta>();
        Consulta cons = new Consulta(fcon, "Estado general bueno, niÃ±o sano", true, 5, 63, 40, 4);
        consultas.add(cons);
        LocalDateTime fcon1 = LocalDateTime.of(2014, Month.NOVEMBER, 20, 10, 21);
        LocalDateTime fcon10 = LocalDateTime.of(2014, Month.NOVEMBER, 25, 10, 21);
        LocalDateTime fcon11 = LocalDateTime.of(2014, Month.DECEMBER, 20, 10, 21);
        LocalDateTime fcon12 = LocalDateTime.of(2015, Month.NOVEMBER, 18, 14, 21);
        Consulta cons1 = new Consulta(fcon1, "Todo normal", true, 6, 65, 41, 5);
        Consulta cons4 = new Consulta(fcon10, "creciendo sano", true, 7, 73, 43, 8);
        Consulta cons3 = new Consulta(fcon11, "buena movilidad", true, 8, 78, 44, 9);
        Consulta cons5 = new Consulta(fcon12, "buena movilidad", false, 8, 78, 44, 9);
        consultas.add(cons1);        
        consultas.add(cons4);        
        consultas.add(cons3);   
        consultas.add(cons5);
        /*EsquemaVacuna e = new EsquemaVacuna();
        ArrayList<Vacuna> array = new ArrayList<Vacuna>();
        ArrayList<Vacunacion> avacu = new ArrayList<Vacunacion>();
        Vacunacion vac = new Vacunacion(1, nul, array);
        avacu.add(vac);
        e.setVacunaciones(avacu);*/
        
        EsquemaVacuna e = unSistema.getListaesquemas().get(0);
        
        
        Ninio unNinio = new Ninio("Pablito Horacio", "Giminez Lopez", fnac, "123456", consultas, e);
        
        ArrayList<Consulta> consultas2 = new ArrayList<Consulta>();
        consultas2.addAll(consultas);
        LocalDateTime fcon2 = LocalDateTime.of(2015, Month.NOVEMBER, 19, 16, 30);
        Consulta cons2 = new Consulta(fcon2, "Todo ok", false, 4, 41, 13, 5);
        consultas2.add(cons2);        
        Ninio unNinio2 = new Ninio("Ximena", "Capristo Antunez", fnac1, "112233", consultas2, e);
        unSistema.agregarNinio(unNinio);
        unSistema.agregarNinio(unNinio2);

        return unSistema;
    }

    public static String[] ConsultasProximosSieteDias(Sistema unSistema) {

        ArrayList<String> data = new ArrayList<String>();
        ArrayList<Ninio> ninios = unSistema.getListaninios();
        int cantidadNinios = ninios.size();
        
        boolean showNinioName = false;
        if(cantidadNinios > 1) {
            showNinioName = true;
        }
        
        ArrayList<Consulta> consultas;
        
        Iterator<Ninio> iter = ninios.iterator();
        
        while(iter.hasNext()) {
            Ninio n = iter.next();
            consultas = unSistema.getTodasLasConsultasProxSieteDias(n);
            
            Iterator<Consulta> iter2 = consultas.iterator();
            while(iter2.hasNext()) {
                Consulta c = iter2.next();
                LocalDateTime fechaConsulta = c.getFecha();
                data.add(fechaConsulta.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")) + (showNinioName? " ("+ n.getNombres()+")" : ""));
            }
            
            consultas = null;
        }

        String[] proximas = new String[data.size()];

        if(!data.isEmpty()) {
            Iterator<String> iter3 = data.iterator();
            int i = 0;
            while(iter3.hasNext()) {
                proximas[i] = iter3.next();
                i++;
            }
        }
        
        return proximas;
        
    }

    public static void cargarListaConsultasProximas(JList unaLista, Sistema unSistema) {

        unaLista.removeAll();
        String[] consultas = ConsultasProximosSieteDias(unSistema);
        unaLista.setListData(consultas);

    }
    
    public static void cargarListaVacunasProximas(JList unaLista, Sistema unSistema) {
        unaLista.removeAll();
        String[] vacunas = VacunasProximas(unSistema);
        unaLista.setListData(vacunas);
    }
    
    public static String[] VacunasProximas(Sistema unSistema) {
        ArrayList<String> data = new ArrayList<String>();
        ArrayList<Ninio> ninios = unSistema.getListaninios();
        int cantidadNinios = ninios.size();
        
        boolean showNinioName = false;
        if(cantidadNinios > 1) {
            showNinioName = true;
        }
        
        ArrayList<Vacuna> vacunas;
        
        Iterator<Ninio> iter = ninios.iterator();
        
        while(iter.hasNext()) {
            Ninio n = iter.next();
            
            Vacunacion v = n.getEsquemaVacunacion().getVacunacionParaMes(n.getEdadEnMeses());
            
            if(v.getMes() == -1)
                continue;
            
            vacunas = v.getVacunas();
            Iterator<Vacuna> iter2 = vacunas.iterator();
            while(iter2.hasNext()) {
                Vacuna vac = iter2.next();
                data.add(vac.getNombre() + (showNinioName? " ("+ n.getNombres()+")" : ""));
            }
            
            vacunas = null;
        }

        String[] proximas = new String[data.size()];

        if(!data.isEmpty()) {
            Iterator<String> iter3 = data.iterator();
            int i = 0;
            while(iter3.hasNext()) {
                proximas[i] = iter3.next();
                i++;
            }
        }
        
        return proximas;        
    }
    
    static String[] VacunasDeVacunacion(Vacunacion v) {
        ArrayList<String> data = new ArrayList<String>();
        
        Iterator<Vacuna> iter = v.getVacunas().iterator();
        while(iter.hasNext()) {
            Vacuna vacuna = iter.next();
            data.add(vacuna.toString());
        }
        
        String[] todasVacunasEnVacunacion = new String[data.size()];

        if(!data.isEmpty()) {
            Iterator<String> iter3 = data.iterator();
            int i = 0;
            while(iter3.hasNext()) {
                todasVacunasEnVacunacion[i] = iter3.next();
                i++;
            }
        }        
        
        return todasVacunasEnVacunacion;
        
    }
    
            
    public static int GetPixelesPeso(int valor, boolean esX){
    // 1 mes - 12 pix
    // 1 kilo -17 pix
    //punto inicial (157,526)
        int ret;
        if (esX) {
            ret = 157 + (12 * valor);    
        }else{
            ret = 526 - (17 * valor);
        }                      
        return ret;
    }     
    
    public static int GetPixelesTalla(int valor, boolean esX){
    // 1 mes - 14 pix
    // 5 cm - 26 pix
    //punto inicial (159,553)
        int ret;
        if (esX) {
            ret = 159 + (12 * valor);    
        }else{
            ret = 553 - (24 * valor);
        }                      
        return ret;
    }   
    public static int GetPixelesPC(int valor, boolean esX){
    // 1 mes - 12 pix
    // 1 cm - 23 pix
    //punto inicial (17,563)
        int ret;
        if (esX) {
            ret = 17 + (12 * valor);    
        }else{

            ret = 563 - (37 * valor);
        }                      
        return ret;
    }       
}
