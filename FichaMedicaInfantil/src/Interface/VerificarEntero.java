package Interface;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 * Clase VentanaAltaNinio para dar de alta un niño.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class VerificarEntero extends InputVerifier {

    /**
     * Verificamos que la entrada sea un entero.
     * 
     * @param input
     * @return
     */
    @Override
    public boolean verify(JComponent input) {
        JTextField tf = (JTextField) input;

        boolean okEntero = true;

        try {
            Integer.parseInt(tf.getText());
        }
        catch(NumberFormatException ex) {
            okEntero = false;
        }

        return okEntero;
    }
}
