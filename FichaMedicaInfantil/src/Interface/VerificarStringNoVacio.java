package Interface;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 * Clase VentanaAltaNinio para dar de alta un niño.
 * 
 * @author Pablo Benitez (179924) y Luis Introini (181108)
 */
public class VerificarStringNoVacio extends InputVerifier {

    /**
     * Verifica que el texto no esté vacío.
     * 
     * @param input
     * @return
     */
    @Override
    public boolean verify(JComponent input) {
        JTextField tf = (JTextField) input;

        return !tf.getText().isEmpty();
    }
}