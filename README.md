# LÉAME #

Este repositorio contiene el código fuente y documentos necesarios para implementar el proyecto: _Ficha médica infantil para padres_.

### Herramientas necesarias para correr el proyecto ###

* NetBeans IDE 8.0.2
* Java versión 8 Update 60 (build 1.8.0_60-b27)

### Autores ###

* Luis Introini (181108)
* Pablo Benitez (179924)